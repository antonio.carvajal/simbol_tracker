# Codecov
[![codecov](https://codecov.io/gl/antonio.carvajal/simbol_tracker/branch/main/graph/badge.svg?token=UVQUJAGQD0)](https://codecov.io/gl/antonio.carvajal/simbol_tracker/branch/dev-pre)

# --- Simbol tracker ---

## Starting 🚀

This is a project for a technical test.

## Architecture 🛰

- ### General architecture

![General architecture image](./documentation/screenshots/general_architecture.png?raw=true)

The general architecture is based on a CLI solution which communicates with the [Blockchain exchange API](https://api.blockchain.com/v3/#/unauthenticated/getL3OrderBook), extracts information, stores it in a NoSQL database (MongoDB) and displays it through a WebServices exposing a Rest API


- ### Program aarchitecture local

![Program architecture local image](./documentation/screenshots/program_architecture_local.png?raw=true)

The architecture of the program is developed under the philosophy of "Clean architecture". A layered development, where we focus on defining the models and interfaces from the beginning to later design and develop the implementations and business logic. With this, our program gains scalability and flexibility in the face of changes. It is an ideal design for an architecture based on microservices (It is not the case)

Save-orders: What this command performs is a connection with the client (implemented with requests) to the blockchain exchange API and collects the information of the latest buy and sale orders of a pair of currencies given as an argument. Subsequently, using models based on dataclasses and Pandas, the data obtained is treated, modeling and saving it in the MongoDB database.

Run-api: This command generates a Webservice with a configuration generated from the configuration files and exposes a Rest API developed with [FastAPI](https://fastapi.tiangolo.com/). Once the Webservice is up, we can access it via localhost (or the address that is configured through the URL /docs or /redoc). The treatment of the data used by the API is carried out with models based on Pydantic, Dataclasses and Pandas


- ### Program architecture docker

![Program architecture local image](./documentation/screenshots/program_architecture_docker.png?raw=true)

It is the same as the previous architecture, the only difference is that in this case a container layer with Docker-compose has been added. Two containers are raised, one for the database and one for the program. In this case, the direct option of dealing with the CLI is lost **(by default, when the APP container is up, the information of the lasts buy and sell orders of the "BTC-USD" pair is saved)**. We can also access to the Webservice through our localhost (or address defined in the configuration files)


## Pre-requirements 📋
- [Python](https://www.python.org/) >= 3.10 (due to a syntax with Match case)
- [Docker](https://www.docker.com/)


## installation 🔧

## --- For local use ---

- You must first install all the required libraries with the following command: `python.exe -m pip install -r .\requirements.txt`
- You must have a container with your MongoDB database started. For this you must first download the image with the following command: `docker pull mongo`. Now you should start an instance of your container `docker run -d -p 27017:27017 --name my_mongo -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=superPassw0rd -e MONGO_INITDB_DATABASE=tracker mongo` With this command we will expose port 27017 of the container to our local machine through the same port (MongoDB's default port), we will also assign a user and passwords to a database that we will create "tracker" by default.

**Note: If you want to change the input parameters when generating the container, remember to modify the configuration file "Storage.yml"**


## --- For Docker use --- ⛴

Go to the root directory of the project, execute the following command `docker compose up`. Once the containers have been lifted, you can access the API using the following address [Local API doc](http:127.0.0.1:8080/docs) or [Local API redoc](http:127.0.0.1:8080/redoc)


## Wiki 📖
Symbol tracker is a CLI program developed in python.


### Command list 📋

- --help (Show the help message)
- save-orders <Symbol> (Save the information of the last purchase and sale orders)
- run-api (Create a WebService and expose an API to obtain statistics on saved purchase and sale orders)

Run the command `python main.py --help` to see all the information about the program

![Help command image](./documentation/screenshots/help_command.png?raw=true)


The first thing to do is save all the orders of a symbol, for this we will use the command "(save-orders <Symbol>)". For example running the following command: `python main.py save-orders BTC-USD`

![Save-orders command image](./documentation/screenshots/save-orders_command.png?raw=true)


Next, executing the command `python main.py run-api` will raise the webservice with the API that you can access using the link provided by the command itself

![Save-orders command image](./documentation/screenshots/run-api_command.png?raw=true)


From the link [Local API doc](http:127.0.0.1:8080/docs) or [Local API redoc](http:127.0.0.1:8080/redoc) you can access each endpoint to see the models and their definitions

![Save-orders command image](./documentation/screenshots/api_doc.png?raw=true)



## Endpoints 🔗

In this section each endpoint will be explained


- ### GET /statistic/bid/{symbol}/


Parameters: <Symbol>, it is a currency pair eg: "BTC-USD"
Returns the average value of the buy orders, the buy with the highest value, the buy with the lowest value, the total price and the total amount of coins

Example response model
```json
{
  "bids": {
    "average_value": 13747.091,
    "greater_value": {
      "px": 19080,
      "qty": 4.03,
      "num": 562966541440335,
      "value": 76895.96
    },
    "lesser_value": {
      "px": 10000,
      "qty": 0.001,
      "num": 562964987166611,
      "value": 6.333
    },
    "total_qty": 90.407,
    "total_px": 1664158
  }
}
```


- ### GET /statistic/ask/{symbol}/

Parameters: <Symbol>, it is a currency pair eg: "BTC-USD"
Returns the average value of the sell orders, the sell with the highest value, the sell with the lowest value, the total price and the total amount of coins

Example response model
```json
{
  "asks": {
    "average_value": 15410.968,
    "greater_value": {
      "px": 6789572,
      "qty": 0.067,
      "num": 562965801292742,
      "value": 453066.646
    },
    "lesser_value": {
      "px": 21000,
      "qty": 0.001,
      "num": 562966439673401,
      "value": 10.5
    },
    "total_qty": 54.071,
    "total_px": 9220793
  }
}
```


- ### GET /statistic/{symbol}/

Parameters: <Symbol>, it is a currency pair eg: "BTC-USD"
Returns the general statistics of both buy and sell orders for the given currency pair as a parameter.

- Number of purchase orders.
- Number of sales orders.
- Total value of purchase orders.
- Total value of sales orders.
- The total currencies of the purchase orders.
- The total currencies of the sell orders.

Example response model
```json
{
  "symbol": "BTC-USD",
  "bids": {
    "count": 98,
    "qty": 90.407,
    "value": 1347214.872
  },
  "asks": {
    "count": 98,
    "qty": 54.071,
    "value": 1510274.881
  }
}
```


## Configuration ⚙

All configuration is externalized to yml files.

Important Note ‼ If you modify the name of the root directory, you must modify the constant "ROOT_DIR_NAME" found in the module "configuration_store.py" in the path: "business_logic/config/configuration_store.py".

Note: For the settings to take effect, you must restart the program.


### API configuration file:
```yaml
exception_handler_instance: ExceptionHandler
apiapp_instance: FastAPIApp
config_instance: UvicornConfig
app:
  title: StatisticsTracker
  version: '1.0.0'
  origins_url: ["*"]
  ip: '0.0.0.0'
  port: 8080
```

- exception_handler_instance: Refers to the name of the class which implements the interface for handling exception handlers.
- apiapp_instance: Refers to the name of the class which implements the API logic.
- config_instance: Refers to the name of the class which implements the Webservice config.
- title: Refers to the name that your API will have
- version: API Version
- origins_url: Which source addresses will have access to your API
- io: The url that will expose
- port: The port on which the API will listen


### Client configuration file:
```yaml
client_instance: BlockchainExchangeClient
blockchain:
  base_url: api.blockchain.com/v3/exchange/
```

- client_instance: Refers to the name of the class which implements the Blockchain exchange client.
- base_url: URL blockchain API


### Storage configuration file:
```yaml
instance: MongoConnector
mongo:
  username: admin
  password: superPassw0rd
  host: "tracker-db"
  port: 27017
  db_name: "tracker"
  bids_collection_name: bids-col
  asks_collection_name: asks-col
```

- instance: Refers to the name of the class which implements the storage logic.
- username: The username database
- password: The password database
- host: The host on which your database will listen
- port: The port on which your database will listen
- db_name: The database name
- bids_collection_name: The bids collection name
- asks_collection_name: The asks (sell orders) collection name
