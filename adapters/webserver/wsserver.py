import logging
import uvicorn

from typing import List

from business_logic.interfaces.iconfig import IConfig
from business_logic.interfaces.iapiapp import IAPIApp
from fastapi import APIRouter


logger = logging.getLogger('simboltracker')


class WSServer():

    def __init__(
                self,
                app: IAPIApp,
                app_config: IConfig,
                routers: List[APIRouter],
            ):
        self._set_routers(routers)
        self._set_app(app.get_app_instance())
        self.app_config = app_config
        self._set_config(self.app_config.get_config_instance())
        for router in self.routers:
            self.app.include_router(router)
        self.server = uvicorn.Server(self.config)

    def _set_routers(self, routers: List[APIRouter]) -> None:
        if routers:
            self.routers = routers

    def _set_app(self, app: IAPIApp) -> None:
        if app:
            self.app = app

    def _set_config(self, config: IConfig) -> None:
        if config:
            self.config = config

    def run(self) -> None:
        try:
            logger.info(f'WebService: {self.app.title} started')
            self.server.run()
        except Exception as e:
            logger.error(f'Can´t start WebService: {self.app.title}')
            raise e
