from typing import List

from adapters.api.fastapi.apiapp import APIApp
from business_logic.interfaces.ihandlerexceptions import IHandlerException


class FastAPIApp(APIApp):

    def __init__(
            self,
            title,
            version,
            cors_origin_url: List[str] = None,
            handler_exceptions: List[IHandlerException] = None
        ) -> None:
        super().__init__(title, version, cors_origin_url, handler_exceptions)
