from business_logic.interfaces.iconfig import IConfig
from business_logic.interfaces.iapiapp import IAPIApp

from uvicorn.config import Config


class UvicornConfig(IConfig):
    """ UvicornConfig class. log_level: error --> no input messages appear, info --> input messages appear"""
    def __init__(self, app: IAPIApp, bind_host: str, bind_port: str, log_level: str = None):
        self.bind_host = bind_host
        self.bind_port = bind_port
        self.log_level = log_level
        self._set_config(Config(app.get_app_instance(), host=self.bind_host, port=self.bind_port, log_level=self.log_level))

    def get_bind_host(self) -> str:
        return self.bind_host

    def get_bind_port(self) -> str:
        return self.bind_port

    def get_log_level(self) -> str:
        return self.log_level

    def get_config_instance(self) -> Config:
        return self.config

    def _set_config(self, config: Config):
        self.config = config