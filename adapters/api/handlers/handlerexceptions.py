from adapters.api.exceptions import NotFoundError, ObtainProblemError
from fastapi import Request
from fastapi.responses import JSONResponse


async def not_found_exp_handler(request: Request, nfe: NotFoundError):
    return JSONResponse(
        status_code=nfe.status_code,
        content={'message': nfe.message}
    )


async def obtain_problem_exp_handler(request: Request, ope: ObtainProblemError):
    return JSONResponse(
        status_code=ope.status_code,
        content={'message': ope.message}
    )
