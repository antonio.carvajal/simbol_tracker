from typing import Any
from business_logic.interfaces.ihandlerexceptions import IHandlerException


class ExceptionHandler(IHandlerException):
    """Auxiliar class for represent a Handler"""
    exception: Exception
    handler: Any

    def __init__(self, exception: Exception, handler: Any) -> None:
        self.exception = exception
        self.handler = handler

    def get_exception(self):
        return self.exception

    def get_handler(self):
        return self.handler
