import logging

from fastapi import Path, APIRouter, HTTPException, status
from fastapi import Request as FAPIRequest
from adapters.api.exceptions import NotFoundError, ObtainProblemError
from adapters.api.models.bidstatistics import BidStatistics
from adapters.api.models.askstatistics import AskStatistics
from adapters.api.models.statistics import GeneralStatistics
from adapters.api.bridges import statisticsbridge
from adapters.api.security.audit import audit_request


router = APIRouter()
logger = logging.getLogger('simboltracker')


@router.get("/statistic/bid/{symbol}/", tags=["CryptoInfo"], status_code=status.HTTP_200_OK)
def get_bid_statistics_symbol(
        fastapi_request: FAPIRequest,
        symbol: str = Path(...)
        ) -> BidStatistics:
    audit_request(fastapi_request.client.host, "Get bid statistics")
    try:
        bids_statistics: BidStatistics = statisticsbridge.get_bids_statistics(symbol)
    except (NotFoundError, ObtainProblemError, HTTPException) as e:
        raise e
    except Exception as e:
        logger.error(f"An unexpected error has occurred {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return bids_statistics


@router.get("/statistic/ask/{symbol}/", tags=["CryptoInfo"], status_code=status.HTTP_200_OK)
def get_ask_statistics_symbol(
        fastapi_request: FAPIRequest,
        symbol: str = Path(...)
        ) -> AskStatistics:
    audit_request(fastapi_request.client.host, "Get asks statistics")
    try:
        asks_statistics: AskStatistics = statisticsbridge.get_asks_statistics(symbol)
    except (NotFoundError, ObtainProblemError, HTTPException) as e:
        raise e
    except Exception as e:
        logger.error(f"An unexpected error has occurred {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return asks_statistics


@router.get("/statistic/{symbol}/", tags=["CryptoInfo"], response_model=GeneralStatistics, status_code=status.HTTP_200_OK)
def get_general_statistics_symbol(
        fastapi_request: FAPIRequest,
        symbol: str = Path(...)
        ) -> GeneralStatistics:
    audit_request(fastapi_request.client.host, "Get general statistics")
    try:
        general_statistics: GeneralStatistics = statisticsbridge.get_general_statistics(symbol)
    except (NotFoundError, ObtainProblemError, HTTPException) as e:
        raise e
    except Exception as e:
        logger.error(f"An unexpected error has occurred {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Request response: FAIL")
    return general_statistics
