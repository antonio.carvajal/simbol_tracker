from pydantic import BaseModel
from adapters.api.models.statistics import Statistics


class BidStatistics(BaseModel):
    """BidStatistics class that represent a Bid statistics model"""
    bids: Statistics
