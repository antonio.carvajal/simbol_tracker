from pydantic import BaseModel, Field
from typing import Union

from models.bid import Bid
from models.sellorder import SellOrder


class Statistics(BaseModel):
    """Statistics class that represent a statistics model"""
    average_value: float = Field(..., example=0.0)
    greater_value: Union[Bid, SellOrder] = Field(..., example=Bid(px=100.0, qty=100.0, num=5))
    lesser_value: Union[Bid, SellOrder] = Field(..., example=Bid(px=10.0, qty=10.0, num=5))
    total_qty: float = Field(..., example=0.0)
    total_px: int = Field(..., example=0)


class TotalStatistics(BaseModel):
    """TotalStatistics class that represent a total statistics model"""
    count: int = Field(..., example=0)
    qty: float = Field(..., example=0.0)
    value: float = Field(..., example=0.0)


class GeneralStatistics(BaseModel):
    """GeneralStatistics class that represent a general statistics model"""
    symbol: str = Field(..., example="BTC-USD")
    bids: TotalStatistics
    asks: TotalStatistics
