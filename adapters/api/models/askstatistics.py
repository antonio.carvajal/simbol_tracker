from pydantic import BaseModel
from adapters.api.models.statistics import Statistics


class AskStatistics(BaseModel):
    """AskStatistics class that represent a asks statistics model"""
    asks: Statistics
