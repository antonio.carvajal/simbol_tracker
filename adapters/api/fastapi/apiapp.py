from typing import List

from fastapi.applications import FastAPI
from business_logic.interfaces.ihandlerexceptions import IHandlerException
from fastapi.middleware.cors import CORSMiddleware
from business_logic.interfaces.iapiapp import IAPIApp


class APIApp(IAPIApp):

    def __init__(
            self,
            title,
            version,
            cors_origin_url: List[str] = None,
            handler_exceptions: List[IHandlerException] = None
            ):
        self.title = title
        self.version = version
        self._set_app(FastAPI(title=self.title, version=self.version))
        if cors_origin_url:
            self.app.add_middleware(
                CORSMiddleware,
                allow_origins=cors_origin_url,
                allow_credentials=True,
                allow_methods=["*"],
                allow_headers=["*"],
            )
        if handler_exceptions:
            for handler_exception in handler_exceptions:
                self.app.add_exception_handler(handler_exception.get_exception(), handler_exception.get_handler())

    def get_app_instance(self) -> FastAPI:
        return self.app

    def get_title(self) -> str:
        return self.title

    def get_version(self) -> str:
        return self.version

    def _set_app(self, app: FastAPI) -> None:
        self.app = app

    def include_router(self, router) -> None:
        self.app.include_router(router)
