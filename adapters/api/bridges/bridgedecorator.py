from business_logic.config.configuration_store import ConfigurationStore
from adapters.storage import dbconnectors


store = ConfigurationStore()


def db_setup(call_funct):
    def decorator_funct(*args, **kwargs):
        # Pre decorator
        db_instance = getattr(dbconnectors, store.get("storage.instance"))
        db_con = db_instance()
        db_con.connect()
        result = call_funct(db_con=db_con, *args, **kwargs)
        # Post decorator
        db_con.disconnect()
        return result
    return decorator_funct
