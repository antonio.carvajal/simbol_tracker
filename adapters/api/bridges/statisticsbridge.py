import logging
import pandas as pd

from fastapi import HTTPException, status
from adapters.api.exceptions import NotFoundError, ObtainProblemError
from adapters.storage.exceptions import ConnectionError, ItemNotFound, StorageError
from adapters.api.bridges.bridgedecorator import db_setup
from adapters.api.models.bidstatistics import BidStatistics
from adapters.api.models.askstatistics import AskStatistics
from adapters.api.models.statistics import Statistics, GeneralStatistics, TotalStatistics
from models.exceptions import StorageError
from models.ordersinfo import BidsInfo, SellOrdersInfo
from models.bid import Bid


logger = logging.getLogger('simboltracker')
ROUND = 3


@db_setup
def get_bids_statistics(symbol: str, db_con=None) -> BidStatistics:
    try:
        bids_info: BidsInfo = db_con.get_bids_info(symbol)
        df_bids = pd.DataFrame(bids_info.bids)

        s_bids_value = df_bids["value"]
        mean_bids = s_bids_value.mean().round(ROUND)
        max_bids = df_bids.loc[df_bids['value'].idxmax()].round(ROUND)
        min_bids = df_bids.loc[df_bids['value'].idxmin()].round(ROUND)
        total_px = df_bids['px'].sum().round(decimals=ROUND)
        total_qty = df_bids['qty'].sum().round(decimals=ROUND)

        bids_statistics = BidStatistics(
            bids=Statistics(
                average_value=mean_bids,
                greater_value=Bid.from_dict(max_bids),
                lesser_value=Bid.from_dict(min_bids),
                total_qty=total_qty,
                total_px=total_px
            )
        )
    except ConnectionError as e:
        logger.error(f"Error getting the bids statistics: {e.message}")
        raise ObtainProblemError(message=f"Error getting the bids statistics: {e.message}")
    except ItemNotFound:
        logger.error(f"Symbol {symbol} not found in the database")
        raise NotFoundError(f"Symbol {symbol} not found")
    except (StorageError, Exception) as e:
        logger.error(f"Unexpected error occurred getting bids statistics: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception getting bids statistics: {repr(e)}")

    return bids_statistics


@db_setup
def get_asks_statistics(symbol: str, db_con=None) -> AskStatistics:
    try:
        asks_info: SellOrdersInfo = db_con.get_asks_info(symbol)
        df_asks = pd.DataFrame(asks_info.sellorders)

        s_asks_value = df_asks["value"]
        mean_asks = s_asks_value.mean().round(ROUND)
        max_asks = df_asks.loc[df_asks['value'].idxmax()].round(ROUND)
        min_asks = df_asks.loc[df_asks['value'].idxmin()].round(ROUND)
        total_px = df_asks['px'].sum().round(decimals=ROUND)
        total_qty = df_asks['qty'].sum().round(decimals=ROUND)

        asks_statistics = AskStatistics(
            asks=Statistics(
                average_value=mean_asks,
                greater_value=Bid.from_dict(max_asks),
                lesser_value=Bid.from_dict(min_asks),
                total_qty=total_qty,
                total_px=total_px
            )
        )
    except ConnectionError as e:
        logger.error(f"Error getting the asks statistics: {e.message}")
        raise ObtainProblemError(message=f"Error getting the asks statistics: {e.message}")
    except ItemNotFound:
        logger.error(f"Symbol {symbol} not found in the database")
        raise NotFoundError(f"Symbol {symbol} not found")
    except (StorageError, Exception) as e:
        logger.error(f"Unexpected error occurred getting asks statistics: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception getting asks statistics: {repr(e)}")

    return asks_statistics


@db_setup
def get_general_statistics(symbol: str, db_con=None) -> GeneralStatistics:
    try:
        asks_info: SellOrdersInfo = db_con.get_asks_info(symbol)
        bids_info: BidsInfo = db_con.get_bids_info(symbol)
        df_asks = pd.DataFrame(asks_info.sellorders)
        df_bids = pd.DataFrame(bids_info.bids)

        general_statistics = GeneralStatistics(
            symbol=symbol,
            bids=TotalStatistics(
                count=len(df_bids),
                qty=df_bids["qty"].sum().round(decimals=ROUND),
                value=df_bids["value"].sum().round(decimals=ROUND)
            ),
            asks=TotalStatistics(
                count=len(df_asks),
                qty=df_asks["qty"].sum().round(decimals=ROUND),
                value=df_asks["value"].sum().round(decimals=ROUND),
            )
        )

    except ConnectionError as e:
        logger.error(f"Error getting the general statistics: {e.message}")
        raise ObtainProblemError(message=f"Error getting the general statistics: {e.message}")
    except ItemNotFound:
        logger.error(f"Symbol {symbol} not found in the database")
        raise NotFoundError(f"Symbol {symbol} not found")
    except (StorageError, Exception) as e:
        logger.error(f"Unexpected error occurred getting general statistics: {repr(e)}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception getting general statistics: {repr(e)}")

    return general_statistics
