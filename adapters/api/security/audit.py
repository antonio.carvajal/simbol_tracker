import logging


logger = logging.getLogger('simboltracker')


def audit_request(host: str, function_name: str) -> None:
    """
    Logs origin and username of any API KEY access

    :param host: The origin hostname
    :param function_name: The function being accessed
    """
    logger.info(f"Access granted from: {host} to function {function_name}")
