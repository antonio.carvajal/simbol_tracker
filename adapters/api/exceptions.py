from fastapi import status
from business_logic.exceptions import APIError


class NotFoundError(APIError):

    def __init__(self, message: str, status_code: int = status.HTTP_404_NOT_FOUND):
        self.message = message
        self.status_code = status_code
        super().__init__(self.message)

    def __str__(self):
        return repr(self.message)


class ObtainProblemError(APIError):

    def __init__(self, message: str, status_code: int = status.HTTP_500_INTERNAL_SERVER_ERROR):
        self.message = message
        self.status_code = status_code
        super().__init__(self.message)

    def __str__(self):
        return repr(self.message)
