from business_logic.exceptions import APIClientError

class BlockchainAPIClientError(APIClientError):

    def __init__(self, message: str) -> None:
        super().__init__(message)


class SymbolNotFoundError(BlockchainAPIClientError):

    def __init__(self, message: str, symbol: str) -> None:
        super().__init__(message)
        self.symbol = symbol
