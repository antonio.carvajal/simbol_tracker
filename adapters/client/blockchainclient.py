import logging

from requests.exceptions import HTTPError

from business_logic.exceptions import APIClientError
from adapters.client.baseclient import BaseClient
from adapters.client.exceptions import BlockchainAPIClientError, SymbolNotFoundError
from models.ordersinfo import OrdersInfo
from business_logic.interfaces.iapiclient import IApiClient


logger = logging.getLogger('simboltracker')


class BlockchainExchangeClient(IApiClient, BaseClient):

    def __init__(self):
        super().__init__()

    def get_orders_info(self, symbol: str) -> OrdersInfo:
        try:
            logger.info("getting bids from blockchain exchange")
            url = f"https://{self.base_url}l3/{symbol}"
            response = self._response(url)
            response.raise_for_status()
            orders: OrdersInfo = OrdersInfo.from_dict(response.json())
        except APIClientError as apie:
            logger.error(f"Error: Api client error occurs: {apie.message}")
            raise BlockchainAPIClientError(apie.message)
        except HTTPError as hte:
            logger.error(f"Error: The symbol {symbol} not found")
            raise SymbolNotFoundError(f"Symbol {symbol} not found", symbol=symbol)
        return orders
