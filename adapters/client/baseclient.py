import requests
import logging

from business_logic.config.configuration_store import ConfigurationStore
from business_logic.exceptions import APIClientError


logger = logging.getLogger('simboltracker')
store = ConfigurationStore()


class BaseClient:
    def __init__(self):
        self.session = requests.Session()
        self.base_url = store.get("client.blockchain.base_url")

    def _response(self, url: str, method="GET", data=None):
        """ Uses HTTP GET, POST, PUT or DELETE

        Args:
            url (str): The url to use
            request_type (str): The HTTP method to use - GET, POST, PUT or DELETE
            data (dict): (optional) The data insert

        Returns:
            Request (object): if successful, the response from the server
        """

        try:
            logger.info(f"Blockchain exchange API request: {url}")
            logger.debug("Blockchain exchange API data: " + str(data))
            if method == "GET":
                response = self.session.get(url=url, data=data, headers={"accept": "application/json"})
            elif method == "POST":
                response = self.session.post(url=url, data=data)
            elif method == "DELETE":
                response = self.session.delete(url=url, data=data)
            else:
                response = self.session.put(url=url, data=data)
        except (requests.exceptions.RequestException) as e:
            logger.error("Blockchain exchange API connection error: " + repr(e))
            raise ConnectionError("Unable to connect to the Blockchain exchange API")
        except Exception as e:
            logger.critical(f"Blockchain exchange API error: {repr(e)}")
            raise APIClientError(f"Blockchain exchange error: {repr(e)}")

        return response
