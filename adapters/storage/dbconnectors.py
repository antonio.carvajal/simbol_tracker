from adapters.storage.no_sql.mongoconnector import BaseMongoConnector


class MongoConnector(BaseMongoConnector):

    def __init__(self) -> None:
        super().__init__()
