import traceback
import logging
from typing import List

from business_logic.interfaces.istorage import IStorage
from business_logic.config.configuration_store import ConfigurationStore
from adapters.storage.exceptions import ConnectionError, ItemNotFound
from pymongo import MongoClient
from pymongo.errors import InvalidOperation
from models.ordersinfo import BidsInfo, SellOrdersInfo


logger = logging.getLogger('simboltracker')
store = ConfigurationStore()


class BaseMongoConnector(IStorage):

    def __init__(self):
        self.client = None
        self.db = None
        self.logger = logger
        self.bids_col = store.get("storage.mongo.bids_collection_name")
        self.asks_col = store.get("storage.mongo.asks_collection_name")
        self.collections: List[str] = [self.bids_col, self.asks_col]
        self.uri = f"mongodb://{store.get('storage.mongo.username')}:{store.get('storage.mongo.password')}" \
                   f"@{store.get('storage.mongo.host')}:{store.get('storage.mongo.port')}"
        self.connect()
        self._create_collections()
        self.disconnect()

    def connect(self):
        if self.is_up():
            self.logger.debug("DB Is already connected")
        else:
            try:
                self.logger.debug("Trying to connect to DB")
                self.client = MongoClient(self.uri)
                self.db = self.client[store.get('storage.mongo.db_name')]
                self.logger.debug("DB connected")
                self.is_up()
            except Exception as e:
                self.logger.error(f"Cannot connect to database: {repr(e)}")
                raise ConnectionError(f"Cannot connect to database: {repr(e)}")
    
    def _create_collections(self) -> None:
        for collection in self.collections:
            if self.db[collection] == None:
                self.db.create_collection(collection).create_index("symbol", unique=True)

    def disconnect(self):
        if self.client:
            self.client.close()
        self.logger.debug("Disconnected from DB")

    def is_up(self) -> bool:
        if not self.client:
            return False
        try:
            server_info = self.client.server_info()
        except InvalidOperation:
            return False
        if "ok" not in server_info:
            return False
        return self.client.server_info()["ok"] == 1

    def add_document(self, document: dict, coll: str) -> None:
        self.logger.info("Adding document to DB")
        self.db[coll].insert_one(document)

    def get_document(self, document_id: str, coll: str) -> dict:
        self.logger.info(f"Getting document from DB with index {document_id}")
        document = self.db[coll].find_one({"symbol": document_id})
        if not document:
            raise ItemNotFound(f"Item with symbol {document_id!r} not found")
        return document
    
    def get_bids_info(self, symbol: str) -> BidsInfo:
        self.logger.info(f"Getting bids info from DB with symbol {symbol}")
        bids_info: BidsInfo = self.db[self.bids_col].find_one({"symbol": symbol})
        if not bids_info:
            raise ItemNotFound(f"There is no information about bids for the symbol {symbol!r}")
        return BidsInfo.from_dict(bids_info)
    
    def get_asks_info(self, symbol: str) -> SellOrdersInfo:
        self.logger.info(f"Getting sell orders info from DB with symbol {symbol}")
        asks_info: SellOrdersInfo = self.db[self.asks_col].find_one({"symbol": symbol})
        if not asks_info:
            raise ItemNotFound(f"There is no information about sell orders for the symbol {symbol!r}")
        return SellOrdersInfo.from_dict(asks_info)
    
    def document_exists(self, document_id: str, coll: str) -> dict:
        try:
            self.get_document(document_id, coll)
        except ItemNotFound:
            self.logger.warning(f"Documment with index {document_id} in {coll} collection not exists")
            return False
        return True
