from models.exceptions import StorageError


class ConnectionError(StorageError):
    def __init__(self, message: str) -> None:
        super().__init__(message)


class ItemNotFound(StorageError):
    def __init__(self, message: str) -> None:
        super().__init__(message)
