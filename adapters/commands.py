import logging
import pandas as pd
from rich.console import Console

from adapters import client
from adapters.client.exceptions import SymbolNotFoundError
from adapters.storage import dbconnectors
from adapters.storage.exceptions import ConnectionError
from adapters.webserver.wsserver import WSServer
from adapters.api.handlers import handlersimplements
from adapters.api.exceptions import NotFoundError, ObtainProblemError
from adapters.api.handlers.handlerexceptions import not_found_exp_handler, obtain_problem_exp_handler
from adapters.api.config import configimplements
from adapters.api.endpoints import statistics_ep 
from adapters.api import apiapps

from business_logic.interfaces.iapiclient import IApiClient
from business_logic.config.configuration_store import ConfigurationStore
from business_logic.interfaces.istorage import IStorage

from models.ordersinfo import BidsInfo, OrdersInfo, SellOrdersInfo


logger = logging.getLogger('simboltracker')
store = ConfigurationStore()
console = Console()


def print_title() -> None:
    console.print(f"\n\n[green] ____  __  _  _  ____   __   __      ____  ____   __    ___  __ _  ____  ____\n"
          f"/ ___)(  )( \/ )(  _ \ /  \ (  )    (_  _)(  _ \ / _\  / __)(  / )(  __)(  _ \\\n"
          f"\___ \ )( / \/ \ ) _ ((  O )/ (_/\    )(   )   //    \( (__  )  (  ) _)  )   /\n"
          f"(____/(__)\_)(_/(____/ \__/ \____/   (__) (__\_)\_/\_/ \___)(__\_)(____)(__\_)[/green]\n\n")


def save_orders(symbol: str):
    """
    Find the latest open buy and sell orders for a specific pair (Symbol)
    
    Example (save-orders BTC-USD)
    """
    try:
        print_title()
        console.rule("[red]Step 1[/red] Get Information")
        db_instance = getattr(dbconnectors, store.get("storage.instance"))
        client_instance = getattr(client, store.get("client.client_instance"))
        bids_coll_name = store.get("storage.mongo.bids_collection_name")
        aks_coll_name = store.get("storage.mongo.asks_collection_name")

        bc_client: IApiClient = client_instance()
        tracker_db: IStorage = db_instance()
        tracker_db.connect()

        if not tracker_db.document_exists(symbol, coll=bids_coll_name) or not tracker_db.document_exists(symbol, coll=aks_coll_name):
            with console.status(f"Gteting the order information for the {symbol} symbol..."):
                order_info: OrdersInfo = bc_client.get_orders_info(symbol)
            console.print("Information obtained successfully :white_check_mark:\n")

            bids_info: BidsInfo = BidsInfo(symbol=symbol, bids=order_info.bids)
            sell_orders_info: SellOrdersInfo = SellOrdersInfo(symbol=symbol, sellorders=order_info.sellorders)

            console.rule("[red]Step 2[/red] Dump Information")
            with console.status(f"Dumping information to a Dataframes..."):
                logger.debug("Dumping information to a Dataframes")
                df_bids = pd.DataFrame([bids_info])
                df_sellorders = pd.DataFrame([sell_orders_info])
            console.print("Information dumped correctly :white_check_mark:\n")
            
            console.rule("[red]Step 3[/red] Save Information")
            if not tracker_db.document_exists(symbol, coll=bids_coll_name):
                for document in df_bids.to_dict('records'):
                    tracker_db.add_document(document, bids_coll_name)
            if not tracker_db.document_exists(symbol, coll=aks_coll_name):
                for document in df_sellorders.to_dict('records'):
                    tracker_db.add_document(document, aks_coll_name)
            console.print(f"Information of the orders saved correctly :white_check_mark:\n")
        else:
            console.print("The information has already been extracted :thumbs_up:\n")

        tracker_db.disconnect()
    except SymbolNotFoundError as sye:
        console.print(f"Not found orders for symbol: {sye.symbol} :cross_mark:")
    except ConnectionError as ce:
        console.print(ce.message)


def run_api():
    """
    Create and launch a WebService to expose the API (You must have elements saved in your database.
    To do this, first execute the command "save-orders <Symbol>")
    """
    print_title()
    console.print("Running API...:shooting_star:\n")
    exception_handler_builder = getattr(handlersimplements, store.get("api.exception_handler_instance"))
    api_app_builder = getattr(apiapps, store.get("api.apiapp_instance"))
    config_builder = getattr(configimplements, store.get("api.config_instance"))

    # Generate a API APP
    api_app = api_app_builder(
        store.get("api.app.title"),
        store.get("api.app.version"),
        cors_origin_url=store.get("api.app.origins_url"),
        handler_exceptions=[
            exception_handler_builder(exception=NotFoundError, handler=not_found_exp_handler),
            exception_handler_builder(exception=ObtainProblemError, handler=obtain_problem_exp_handler)
        ]
    )

    # Generate WebService
    statistics_ws = WSServer(
        app=api_app,
        app_config=config_builder(api_app, store.get("api.app.ip"), store.get("api.app.port")),
        routers=[statistics_ep.router]
    )
    
    # Run WS
    # This is only for correct output
    host = statistics_ws.app_config.bind_host
    if host == "0.0.0.0":
        host = "127.0.0.1"
    console.print(f"WebService: {api_app.title} started --> access the following link to access the API "
          f"[blue underline]http://{host}:{statistics_ws.app_config.bind_port}/docs[/blue underline] :thumbs_up:")
    statistics_ws.run()
