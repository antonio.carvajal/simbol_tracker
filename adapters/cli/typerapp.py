import inspect
import logging

from typer import Typer
from typing import Callable, List, Dict

from models.command import Command
from business_logic.interfaces.iclimaker import ICliMaker


# Podría haber usado composición pasandoselo al constructor de TyperAPP
# pero para mantener el main lo más limpio posible, instancio aquí Typer
app = Typer()
logger = logging.getLogger('simboltracker')


class TyperAPP(ICliMaker):

    def __init__(self, commands: List[Callable]) -> None:
        self.commands: List[Command] = []

        for func in commands:
            self.add_command(func)

    def add_command(self, f: Callable) -> None:
        logger.debug(f"Loading command {f.__name__}")
        self.commands.append(self._make_command(f))
        app.command()(self.commands[-1].func)
    
    def load_commands(self) -> None:
        logger.debug("Loading all commands")
        for command in self.commands:
            app.command()(command.func)
    
    def _make_command(self, call_funct) -> Command:
        kwargs: Dict = {}
        for param, value in inspect.signature(call_funct).parameters.items():
            kwargs[param] = value.default if value.default is not inspect.Parameter.empty else None
        command = Command(call_funct, kargs=kwargs)
        return command

    def run(self) -> None:
        logger.debug("Running CLI")
        app()
