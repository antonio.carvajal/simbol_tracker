FROM python:3.10-slim
WORKDIR /simbol_tracker
COPY ./requirements.txt /code/requirements.txt
RUN pip3 install --no-cache-dir --upgrade -r /code/requirements.txt
COPY ./ /simbol_tracker
EXPOSE 8080
CMD ["python", "main.py", "run-api"]
