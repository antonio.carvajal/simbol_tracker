class APIClientError(Exception):

    def __init__(self, message: str) -> None:
        super().__init__()
        self.message = message


class APIError(Exception):

    def __init__(self, message: str) -> None:
        super().__init__()
        self.message = message
