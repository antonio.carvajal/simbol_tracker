import os
from logging.config import dictConfig
from business_logic.config.configuration_store import ConfigurationStore


def _create_log_dirs(logging_conf_handlers: dict):
    """
    Iterate log handlers and check if output is a file.
    If so, creates file's folder if doesn't exist.

    :param logging_conf_handlers: Dictionary with info of logging handlers.
    """
    for _, sub_dict in logging_conf_handlers.items():
        if 'filename' in sub_dict:
            file_folder = os.path.dirname(sub_dict['filename'])
            if not os.path.exists(file_folder):
                os.makedirs(file_folder)


# Configure logging
def conf_log_system() -> None:
    logging_conf = ConfigurationStore().get('log_config')
    _create_log_dirs(logging_conf['handlers'])
    dictConfig(logging_conf)
