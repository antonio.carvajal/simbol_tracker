import os
from pathlib import Path
import yaml
import jmespath

from business_logic.singleton import SingletonMeta
from models.exceptions import ConfigurationError, LoadConfigurationFilesError


basic_type = str | int | dict | list
ROOT_DIR_NAME: str = 'simbol_tracker'.upper()
_configured_store = {}  # Reference to ConfigurationStore()._store


def _yaml_config_store_constructor(loader, node):
    statement = loader.construct_scalar(node)
    separator = "::"
    value, default_value = statement.split(separator, 2)
    global _configured_store
    return _configured_store.get(value, default_value)


def _yaml_relative_path_constructor(loader, node):
    value = loader.construct_scalar(node)
    abs_path = ConfigurationStore.get_root_project_path() / value
    return f"{abs_path}"


def _yaml_join(loader, node):
    seq = loader.construct_sequence(node)
    return "".join([str(x) for x in seq])


yaml.add_constructor("!CONFIGURED", _yaml_config_store_constructor)
yaml.add_constructor("!PROJECT_RELATIVE_PATH", _yaml_relative_path_constructor)
yaml.add_constructor("!JOIN", _yaml_join)


class ConfigurationStore(metaclass=SingletonMeta):

    def __init__(self):
        """
        Initialize ConfigurationStore
        Steps:
        1. Loads environment variables (some override default to load yaml files)
        2. Loads yaml files configuration
        """
        self._store = _configured_store
        self._initialize()

    def _initialize(self):
        self._load_env_configuration()
        self._load_files_configuration(self.get("config_path", "config/"))

    def get(self, parameter: str, default: basic_type = None) -> basic_type:
        """
        Retrieves the parameter from store and triggers refreshing config if needed
        :param default:
        :param parameter:
        :return:
        """
        return jmespath.search(parameter.lower(), self._store) or default

    @staticmethod
    def get_root_project_path() -> Path:
        """
        Retrieves the absolute path of the project
        :return:
        """
        current_dir = Path(__file__)
        root_path = next(p for p in current_dir.parents if ROOT_DIR_NAME in p.parts[-1].upper())
        if not root_path:
            raise ConfigurationError(f"Unable to find root directory, check if the name is {ROOT_DIR_NAME}")
        return root_path

    def _update_store(self, update_vars: dict):
        """
        Updates the store internal dict with the given variables in dict with lowercase keys
        :param update_vars:
        :return:
        """
        vars_lowercase = {x.lower(): y for x, y in update_vars.items()}
        self._store.update(vars_lowercase)

    def _load_env_configuration(self):
        """
        Loads every environment variable starting with ENV_
        If the variable is already loaded overwrites it
        """
        prefix = f"{ROOT_DIR_NAME}_"
        env_vars = {x.replace(prefix, ""): y for x, y in os.environ.items() if x.startswith(prefix)}
        self._update_store(env_vars)

    def _load_files_configuration(self, path: str):
        """
        Loads every file configuration in the path
        If the variable is already loaded overwrites it
        :return:
        """
        try:
            if Path(path).is_absolute():
                parsed_path = Path(path)
            else:
                parsed_path = self.get_root_project_path() / path

            for file in Path.iterdir(parsed_path):
                if file.suffix in [".yml", ".yaml"]:
                    with open(str(file), 'r') as f:
                        data = yaml.load(f, Loader=yaml.FullLoader)
                        self._update_store({file.stem: data})

        except OSError as e:
            raise LoadConfigurationFilesError(f"Unable to read configuration YAML files from path {path}: {e.strerror}")
