from abc import ABCMeta, abstractmethod


class IConfig(metaclass=ABCMeta):
    bind_host: str
    bind_port: str

    @abstractmethod
    def get_bind_host(self) -> str:
        raise NotImplementedError

    @abstractmethod
    def get_bind_port(self) -> str:
        raise NotImplementedError

    @abstractmethod
    def get_config_instance(self):
        raise NotImplementedError

    @abstractmethod
    def _set_config(self, config) -> None:
        raise NotImplementedError
