from abc import ABC, abstractmethod
from typing import Callable


class ICliMaker(ABC):
    
    @abstractmethod
    def run(self) -> None:
        raise NotImplementedError
    
    @abstractmethod
    def add_command(self, f: Callable) -> None:
        raise NotImplementedError
    
    @abstractmethod
    def load_commands(self) -> None:
        raise NotImplementedError
