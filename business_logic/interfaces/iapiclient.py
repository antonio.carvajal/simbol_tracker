from abc import ABC, abstractmethod

from models.ordersinfo import OrdersInfo


class IApiClient(ABC):
    
    @abstractmethod
    def get_orders_info(self, symbol: str) -> OrdersInfo:
        raise NotImplementedError
