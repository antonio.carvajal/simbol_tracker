from abc import ABCMeta, abstractmethod


class IAPIApp(metaclass=ABCMeta):
    title: str
    version: str

    @abstractmethod
    def get_app_instance(self):
        raise NotImplementedError

    @abstractmethod
    def get_title(self):
        raise NotImplementedError

    @abstractmethod
    def get_version(self):
        raise NotImplementedError

    @abstractmethod
    def _set_app(self, app) -> None:
        raise NotImplementedError

    @abstractmethod
    def include_router(self, router) -> None:
        raise NotImplementedError
