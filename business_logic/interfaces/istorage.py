from abc import abstractmethod
from business_logic.singleton import SingletonMeta
from models.ordersinfo import BidsInfo, SellOrdersInfo


# In This case need to set the Singleton metaclass for force to be Singleton
class IStorage(metaclass=SingletonMeta):

    @abstractmethod
    def connect(self):
        raise NotImplementedError

    @abstractmethod
    def disconnect(self):
        raise NotImplementedError

    @abstractmethod
    def is_up(self) -> bool:
        raise NotImplementedError

    @abstractmethod
    def add_document(self, document, coll: str):
        raise NotImplementedError

    @abstractmethod
    def get_document(self, document_id, coll: str):
        raise NotImplementedError
    
    @abstractmethod
    def get_bids_info(self, symbol: str) -> BidsInfo:
        raise NotImplementedError

    @abstractmethod
    def get_asks_info(self, symbol: str) -> SellOrdersInfo:
        raise NotImplementedError

    @abstractmethod
    def document_exists(self, document_id, coll: str):
        raise NotImplementedError
