from abc import ABCMeta, abstractmethod
from typing import Any


class IHandlerException(metaclass=ABCMeta):
    exception: Exception
    handler: Any

    @abstractmethod
    def get_exception(self):
        raise NotImplementedError

    @abstractmethod
    def get_handler(self):
        raise NotImplementedError
