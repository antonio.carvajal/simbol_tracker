import logging
from business_logic.config.stlogger import conf_log_system
from business_logic.config.configuration_store import ConfigurationStore
from business_logic.interfaces.iclimaker import ICliMaker
from adapters.commands import save_orders, run_api
from adapters.cli.typerapp import TyperAPP


# Configure and create logger
conf_log_system()
logger = logging.getLogger('simboltracker')
store = ConfigurationStore()


if __name__ == "__main__":
    commands = [save_orders, run_api]
    app: ICliMaker = TyperAPP(commands)
    app.run()
