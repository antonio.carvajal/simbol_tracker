import json

def get_model_json(file_name: str):
    f = open(f'./tests/models/{file_name}')
    file = json.load(f)
    f.close()
    return file
