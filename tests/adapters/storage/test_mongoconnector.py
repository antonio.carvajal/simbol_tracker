import pytest

from business_logic.interfaces.istorage import IStorage
from adapters.storage.exceptions import ConnectionError, ItemNotFound
from tests.adapters.client.conftest import orders_info_json
from tests.adapters.storage.conftest import orders_info2_json


### Connect Mongo ###
def test_connect(mongo_storage_client: IStorage):
    mongo_storage_client.connect()


def test_still_connect(mongo_storage_client_disconect: IStorage):
    mongo_storage_client_disconect.connect()


def test_connect_error(mock_storage_error, mongo_storage_client_disconect: IStorage):
    with pytest.raises(ConnectionError) as e:
        mongo_storage_client_disconect.connect()
    assert f"Cannot connect to database: Exception()" == e.value.message


### Get document ###
@pytest.mark.parametrize("document_id,coll,doc_expected", [("Test-symbol", "test-coll", orders_info_json), ("Test-symbol2", "test-coll", orders_info2_json)])
def test_get_document_ok(mongo_storage_client_disconect: IStorage, document_id: str, coll: str, doc_expected: dict):
    mongo_storage_client_disconect.connect()
    document = mongo_storage_client_disconect.get_document(document_id, coll)
    assert document == doc_expected


@pytest.mark.parametrize("document_id,coll", [("Test-symbol3", "test-coll"), ("Test-symbol4", "test-coll")])
def test_documment_not_found(mongo_storage_client_disconect: IStorage, document_id: str, coll: str):
    mongo_storage_client_disconect.connect()
    with pytest.raises(ItemNotFound) as e:
        mongo_storage_client_disconect.get_document(document_id, coll)
    assert f"Item with symbol {document_id!r} not found" == e.value.message
