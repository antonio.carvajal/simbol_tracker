import pytest

from pymongo import MongoClient
from pymongo.collection import Collection
from adapters.storage.no_sql.mongoconnector import BaseMongoConnector
from tests.adapters.client.conftest import orders_info_json
from tests.models.readmodels import get_model_json


orders_info2_json = get_model_json("ordersinfo_response2.json")


@pytest.fixture
def mock_storage(monkeypatch):

    def mock_up(*args, **kwargs):
        return True
    
    def mock_create_collections(*args, **kwargs):
        pass

    def mock_get_document(*args, **kwargs):
        return orders_info_json

    monkeypatch.setattr(BaseMongoConnector, "is_up", mock_up)
    monkeypatch.setattr(BaseMongoConnector, "_create_collections", mock_create_collections)
    monkeypatch.setattr(Collection, "find_one", mock_get_document)


@pytest.fixture
def mock_storage_disconnect(monkeypatch):

    def mock_up(*args, **kwargs):
        return False
    
    def mock_create_collections(*args, **kwargs):
        pass

    def mock_get_document(*args, **kwargs):
        match args[1]['symbol']:
            case "Test-symbol":
                return orders_info_json
            case "Test-symbol2":
                return orders_info2_json
            case _:
                return None

    monkeypatch.setattr(BaseMongoConnector, "is_up", mock_up)
    monkeypatch.setattr(BaseMongoConnector, "_create_collections", mock_create_collections)
    monkeypatch.setattr(Collection, "find_one", mock_get_document)


@pytest.fixture
def mock_storage_error(monkeypatch):

    def mock_construct(*args, **kwargs):
        raise Exception()

    monkeypatch.setattr(MongoClient, "__init__", mock_construct)


@pytest.fixture()
def mongo_storage_client(mock_storage):
    return BaseMongoConnector()


@pytest.fixture()
def mongo_storage_client_disconect(mock_storage_disconnect):
    return BaseMongoConnector()
