import pytest

from business_logic.interfaces.iapiclient import IApiClient
from adapters.client.exceptions import BlockchainAPIClientError, SymbolNotFoundError


### GET Response ###
@pytest.mark.parametrize("symbol", ["Test-sybol"])
def test_get_orders(mock_response, blockchain_exchange_client: IApiClient, symbol: str, response_expected):
    response = blockchain_exchange_client.get_orders_info(symbol)
    assert response == response_expected


def test_get_orders_bc_error(mock_response_bc_fails, blockchain_exchange_client: IApiClient):
    with pytest.raises(BlockchainAPIClientError) as e:
        blockchain_exchange_client.get_orders_info("Test-symbol")
    assert "Blockchain exchange error test" == e.value.message


def test_get_orders_500_error(mock_response_500_fails, blockchain_exchange_client: IApiClient):
    with pytest.raises(SymbolNotFoundError) as e:
        blockchain_exchange_client.get_orders_info("Test-symbol")
    assert "Symbol Test-symbol not found" == e.value.message
