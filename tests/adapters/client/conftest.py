import pytest
import requests

from requests.exceptions import HTTPError

from adapters.client.blockchainclient import BlockchainExchangeClient
from adapters.client.baseclient import BaseClient
from business_logic.exceptions import APIClientError
from models.ordersinfo import OrdersInfo
from models.bid import Bid
from models.sellorder import SellOrder
from tests.models.readmodels import get_model_json


orders_info_json = get_model_json("ordersinfo_response.json")


@pytest.fixture(scope="session")
def blockchain_exchange_client():
    return BlockchainExchangeClient()


@pytest.mark.parametrize("symbol", ["Test-sybol"])
@pytest.fixture
def response_expected(symbol):
    return OrdersInfo(
        symbol=symbol,
        bids=[Bid(px=10, qty=10, num=1), Bid(px=2.5, qty=2, num=2)],
        sellorders=[SellOrder(px=10, qty=10, num=1), SellOrder(px=2.5, qty=2, num=2)]
    )


class RequestMockResponse:

    def __init__(self, status_code=200, text=""):
        self.status_code = status_code
        self.text = text

    @staticmethod
    def json():
        return orders_info_json
    
    def raise_for_status(self):
        if self.status_code != 200:
            raise HTTPError()


@pytest.fixture
def mock_response(monkeypatch):

    def mock_get(*args, **kwargs):
        return RequestMockResponse()

    def mock_put(*args, **kwargs):
        return RequestMockResponse(text="Update Ok")
    
    def mock_post(*args, **kwargs):
        return RequestMockResponse(text="Upload Ok")
    
    def mock_delete(*args, **kwargs):
        return RequestMockResponse(text="Delete Ok")

    monkeypatch.setattr(requests.Session, "get", mock_get)
    monkeypatch.setattr(requests.Session, "put", mock_put)
    monkeypatch.setattr(requests.Session, "post", mock_post)
    monkeypatch.setattr(requests.Session, "delete", mock_delete)


@pytest.fixture
def mock_response_bc_fails(monkeypatch):

    def mock_get(*args, **kwargs):
        return RequestMockResponse(500, "Internal server error")
    
    def mock_response(*args, **kwargs):
        raise APIClientError(f"Blockchain exchange error test")

    monkeypatch.setattr(requests.Session, "get", mock_get)
    monkeypatch.setattr(BaseClient, "_response", mock_response)


@pytest.fixture
def mock_response_500_fails(monkeypatch):

    def mock_get(*args, **kwargs):
        return RequestMockResponse(status_code=500)

    monkeypatch.setattr(requests.Session, "get", mock_get)
