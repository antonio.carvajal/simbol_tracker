from dataclasses import dataclass, field
from dataclasses_json import dataclass_json, config

from models.bid import Bid
from models.sellorder import SellOrder


@dataclass_json
@dataclass
class OrdersInfo:
    """Model used to represent a orders info"""
    symbol: str
    bids: list[Bid] = field(default_factory=list)
    sellorders: list[SellOrder] = field(metadata=config(field_name="asks"), default_factory=list)


@dataclass_json
@dataclass
class BidsInfo:
    """Model used to represent a bids info"""
    symbol: str
    bids: list[Bid] = field(default_factory=list)


@dataclass_json
@dataclass
class SellOrdersInfo:
    """Model used to represent a sell orders info"""
    symbol: str
    sellorders: list[SellOrder] = field(metadata=config(field_name="asks"), default_factory=list)
