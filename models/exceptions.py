class StorageError(Exception):
    def __init__(self, message: str) -> None:
        super().__init__()
        self.message = message


class ConfigurationError(Exception):

    def __init__(self, message: str) -> None:
        super().__init__()
        self.message = message


class LoadConfigurationFilesError(ConfigurationError):

    def __init__(self, message: str) -> None:
        super().__init__(message)
