from dataclasses import dataclass
from dataclasses_json import dataclass_json


@dataclass_json
@dataclass
class Bid:
    """Model used to represent a bid."""

    px: float
    qty: float
    num: int
    value: float = 0

    def __post_init__(self):
        if not self.value:
            self.value = self.px * self.qty
