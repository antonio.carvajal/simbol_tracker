from dataclasses import dataclass
from typing import Dict, Callable


@dataclass
class Command:
    """Model used to represent the Command."""
    func: Callable
    kargs: Dict
