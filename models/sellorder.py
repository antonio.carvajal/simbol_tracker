from dataclasses import dataclass
from dataclasses_json import dataclass_json


@dataclass_json
@dataclass
class SellOrder:
    """Model used to represent a 'ask' (sell order)."""

    px: float
    qty: float
    num: int
    value: int = 0
    
    def __post_init__(self):
        self.value = self.px * self.qty
